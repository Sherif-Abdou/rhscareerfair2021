﻿using System;
using System.Collections.Generic;

namespace RHSCode
{
    public class Packager
    {
        public Packager()
        {
        }

        public static double MIXES_PER_MNM = 24/7;
        public static double MIXES_PER_PEANUT = 96 / 7;
        public static double MIXES_PER_RAISIN = 48 / 7;


        public List<(int, int, int)> createMix(int number)
        {
            var values = new List<(int, int, int)>();
            for (int i = 1; i <= number; i++)
            {
                int moz = i;
                int roz = 2 * moz;
                int poz = 4 * moz;
                if (moz % 80 != 0 || poz%100 != 0 || MIXES_PER_MNM*moz % 24 != 0 )
                {
                    continue;
                }
                int m_containers = moz / 80;
                int r_containers = roz / 80;
                int p_containers = poz / 100;
                values.Add((p_containers, r_containers, m_containers));
            }

            return values;
        }
    }
}
