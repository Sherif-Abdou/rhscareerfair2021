﻿using System;

namespace RHSCode
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var packager = new Packager();

            var options = packager.createMix(100000);

            foreach (var option in options)
            {
                var (p, r, m) = option; // peanuts, raisins, mnms
                var price = p * 2.5 + r * 3.75 + m * 4.1;
                if (price > 1000 || Packager.MIXES_PER_MNM*(m*80) < 500)
                {
                    continue;
                }
                //var price = packager.CalculatePrice(option);
                Console.WriteLine("Total supply of peanuts: " + p);
                Console.WriteLine("Total supply of raisis: " + r);
                Console.WriteLine("Total supply of mnm's: " + m);
                Console.WriteLine("Total supply cost: " + price);
                Console.WriteLine("Total trail mix bags that can be made " + Packager.MIXES_PER_MNM * m * 80);
            }
        }
    }
}
